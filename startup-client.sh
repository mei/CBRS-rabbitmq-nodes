sudo add-apt-repository -y ppa:ettusresearch/uhd
sudo add-apt-repository -y ppa:johnsond-u/sdr
sudo apt-get update -y

## Get SDR prerequisites
for thing in $*
do
    case $thing in
        gnuradio)
            sudo DEBIAN_FRONTEND=noninteractive apt-get install -y gnuradio python3-gi gobject-introspection gir1.2-gtk-3.0 python3-cairo python3-gi-cairo
            ;;

        srslte)
            sudo DEBIAN_FRONTEND=noninteractive apt-get install -y srslte
            ;;
    esac
done

sudo sysctl -w net.core.rmem_max=24862979
sudo sysctl -w net.core.wmem_max=24862979

sudo ed /etc/sysctl.conf << "EDEND"
a
net.core.rmem_max=24862979
net.core.wmem_max=24862979
.
w
EDEND

## Get elixir prereqs
sudo apt-get install curl wget gnupg apt-transport-https -y

## Get signing key
wget https://packages.erlang-solutions.com/erlang-solutions_2.0_all.deb && sudo dpkg -i erlang-solutions_2.0_all.deb
sudo apt-get update -y

## Install elixir
sudo apt-get install esl-erlang elixir -y --fix-missing

## Start a new project and set up amqp as a dependency
cd ~ && mix new client

cd client 
rm mix.exs 
tee mix.exs << EOF
def application do
  [applications: [:amqp]]
end
defp deps() do
  [
    {:amqp, "~> 1.0"},
  ]
end
EOF

mix deps get
