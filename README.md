# RabbitMQ CBRS profile

This profile allocated two nodes connected to two seperate SDR rooftop stations. You can select which locations you'd like the rooftop stations to be at. One node will be deployed as a RabbitMQ server and another as a client node.